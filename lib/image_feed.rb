class ImageFeed
  NAME = "image_feed_"

  def self.insert user_id, image_url
    REDIS.lpush(NAME+user_id, image_url)
    REDIS.ltrim(NAME+user_id,0,14)
  end

end