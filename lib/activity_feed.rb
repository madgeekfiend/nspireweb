class ActivityFeed
  FEED_NAME = "activity_feed"
  MAX_CHARACTER = 55

  def self.add_feed msg
    REDIS.lpush(FEED_NAME, msg[0..50])
    #Trim the list to only 10 elements
    REDIS.ltrim(FEED_NAME, 0, 10)
  end

  def self.get_feed
    REDIS.lrange(FEED_NAME, 0, 10)
  end

  def self.add_image_feed_self url, user_id
    REDIS.lpush("#{FEED_NAME}_image_#{user_id}", url)
    REDIS.ltrim("#{FEED_NAME}_image_#{user_id}")
  end

  def self.get_image_feed_self user_id
    REDIS.lrange("#{FEED_NAME}_image_#{user_id}", 0, 20)
  end

  def self.del_image_feed_self user_id
    REDIS.del("#{FEED_NAME}_image_#{user_id}")
  end

end