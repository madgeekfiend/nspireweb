class ImageView
  ALL_TIME = "image_view_all_time"
  MONTH = "image_view_#{Time.new.month}#{Time.new.year}"
  WEEK = "image_view_week_#{Time.new.beginning_of_week.month}#{Time.new.beginning_of_week.day}#{Time.new.beginning_of_week.year}"

  #returns the incremented page view count
  def self.increment id
    REDIS.zincrby(ALL_TIME,1, id)
    REDIS.zincrby(MONTH, 1, id )
    REDIS.zincrby(WEEK, 1, id )
    # Increment image view
    REDIS.incr( "image_views_#{id}")
  end

  def self.remove_all id
    REDIS.zrem(ALL_TIME, id )
    REDIS.zrem(MONTH, id )
    REDIS.zrem(WEEK, id )
    REDIS.del("image_views_#{id}")
  end

  def self.get_view_count id
    REDIS.get("image_views_#{id}")
  end

end