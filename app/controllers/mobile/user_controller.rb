class Mobile::UserController < ApplicationController
layout nil

  def index
    @images = User.first.images.order("created_at DESC")
  end

  def top_nspre
    #24 should be max images that are sent to the user
    @images = User.first.images.order("created_at DESC")
  end

end
