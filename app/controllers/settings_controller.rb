class SettingsController < ApplicationController
  before_filter :authenticate_user!
  layout "application"

  def show

  end

  def edit
    @setting = current_user.setting
  end

  def update
    setting = Setting.find(params[:setting][:id] )
    setting.update_attributes(params[:setting])
    redirect_to edit_setting_url
  end

  def email_show

  end

end
