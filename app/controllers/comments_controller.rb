class CommentsController < ApplicationController

  def create
    @comment = Comment.create!(params[:comment])
    image = @comment.image
    if image.user.setting.newsletter?
      Notifier.delay.comment_added(image.id)
    end
  end

end
