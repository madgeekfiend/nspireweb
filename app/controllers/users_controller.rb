class UsersController < ApplicationController

  def profile
    @user = User.find_by_nickname(params[:id]) || User.find_by_slug(params[:id])
    @images = Image.where(:user_id=>@user).order("created_at DESC").page params[:page]
  end

end
