require "digest"

class Api::MobileController < ApplicationController
  skip_before_filter :verify_authenticity_token

  SEED_KEY = "124ldskdDODSl302DSLFvS89DSfEdsdzz"

  def register

    sig = params[:sig]
    render :nothing=>true if sig.blank?

    if get_md5_sig_for_params(params) == sig
      #Check if the user exists first
      u = User.find_by_email( params[:username])
      if u.present? and u.valid_password?(params[:password])
        render :json=>{ :title=>"Logged In", :message=>"You have logged in successfully. Start uploading pictures.", :token=>u.mobile_token, :slug=>u.slug}, :status=>200
      else
        begin
          #register the user
          u = User.new( :email=>params[:username], :password=>params[:password], :curated=>true )
          u.save!

          render :json=>{ :title=>"Registered", :message=>"You have successfully registered a new account. Please confirm your email to login at http://www.nsprme.com. If you forgot your password go there to retrieve it.", :token => u.mobile_token, :slug=>u.slug }, :status => 200
        rescue ActiveRecord::RecordInvalid => e
          render :json=>{ :title=>"Error", :message=>"#{e.message}. Please try again."}, :status=>203
        end
      end
    else
      render :json=>{ :title=>"Oh oh", :message=>"Unable to create account with this request. Please try again." }, :status => 203
    end

  end

  def image

    sig = params[:sig]
    render :nothing=>true if sig.blank?

    dothis = get_md5_image_upload( params.select { |k,v| %w(token).include?(k) } )
    if dothis == sig
      user = User.find_by_mobile_token(params[:token])

      if not user.blank?
        img = Image.new
        img.front_page = !user.curated?
        img.file = params[:file]
        img.caption = params[:caption]
        img.tag_list = params[:tags]
        img.user_id = user.id
        img.latitude = params[:lat] if params[:lat].present?
        img.longitude = params[:long] if params[:long].present?
        img.save!
      end

      if user.blank?
        render :json=>{:message=>"Error with saving"}, :status=>:ok
      else
        if Rails.env.development?
          base_url = "http://localhost:3000"
        else
          base_url = "http://www.nsprme.com"
        end
        render :json => { :slug=>img.slug, :url=>"#{base_url}#{img.file.thumb.url}", :delete=>img.delete_token }, :status => :ok
      end
    end

  end


  private

  #MD5 signer case insensitive
  def get_md5_sig_for_params params

    #Assume it's a hash
    lower_case = {}
    create_sig = []
    params.each do |k,v|
      lower_case[k.downcase] = v unless k == 'sig' or k == "action" or k == "controller" || k == "file"
    end
    # now sort it
    sorted = lower_case.sort
    sorted.each do |v|
      create_sig << v[1]
    end
    create_sig << SEED_KEY
    sig_from_this = create_sig.join('')

    Digest::MD5.hexdigest(sig_from_this)
  end


  def get_md5_image_upload params
    # This is the format |token|seed|imageUpload| the imageUpload is a string
    sigthis = params["token"] + SEED_KEY + "imageUpload"
    Digest::MD5.hexdigest(sigthis)
  end

end
