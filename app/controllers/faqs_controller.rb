class FaqsController < ApplicationController
  caches_page :index

  def index
    @faqs = Faq.all
  end

end
