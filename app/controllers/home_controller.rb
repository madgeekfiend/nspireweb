class HomeController < ApplicationController

  def index
    @news = News.where(:active=>true).order("created_at DESC")


    @images = Image.where(:front_page=>true).order("created_at DESC").page params[:page]
    @activity = nil #ActivityFeed.get_feed

  end


end
