class SearchController < ApplicationController

  def show
    if params[:terms].blank?
      flash[:notice] = "Please enter a comma separated terms list."
    else
      @terms = params[:terms]
      @images = Image.tagged_with( params[:terms].split(','), :any=>true ).page params[:page]
    end

    flash[:notice] = "No images where found using the terms '#{params[:terms]}'" if @images.blank?
  end
end
