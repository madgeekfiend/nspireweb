class EmailsController < ApplicationController
  before_filter :authenticate_user!

  def edit
    @emails = current_user.emails
  end

  def create
    e = Email.new(params[:email])
    e.verify_token = e.generate_token
    current_user.emails << e
    current_user.save
    # Send out confirmation email
    Notifier.delay.add_email(current_user, e)
    redirect_to edit_email_url
  end

  def destroy
    current_user.emails.destroy params[:email]
    redirect_to edit_email_url
  end

  def verify_other
    @token = params[:token]
    @email = Email.find_by_verify_token(@token)
    @email.verified_at = Time.now
  end

end
