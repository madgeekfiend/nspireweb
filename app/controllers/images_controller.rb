class ImagesController < ApplicationController
  layout "application"
  #skip_before_filter :verify_authenticity_token, :only=>[:create]

  def index
  end

  def create

    user = User.find_by_mobile_token(params[:token])

    if not user.blank?
      img = Image.new
      img.file = params[:file]
      img.user_id = user.id
      img.save!
    end

    if user.blank?
      render :json=>{:message=>"Error with saving"}, :status=>:ok
    else
      if Rails.env.development?
        base_url = "http://localhost:3000"
      end
      render :json => { :slug=>img.slug, :url=>"#{base_url}#{img.file.thumb.url}", :delete=>img.delete_token }, :status => :ok
    end
  end

  def show
    @img = Image.find_by_slug(params[:id] )
    @date = @img.created_at.to_date
    @user = @img.user

    @favorite_count = @img.plusminus
    #@already_favorited = ImageService.has_user_favorited?(@user.id, @img.slug)

    @comments = @img.comments.order('created_at DESC').page params[:page] #@img.comments.order("created_at DESC").limit(100)

    @tags = @img.tag_counts_on(:tags)
    @voters = @img.voters_who_voted[0..19]

    @view_count = ImageView.increment @img.id

    render "show"
  end

  def thumb
    @image = Image.find_by_slug(params[:slug])
  end

  def favorited
    img = Image.find_by_slug(params[:id] )

    ImageService.addFavorite(img, current_user)

    if img.user.setting.newsletter?
      Notifier.delay.send_inspired(img.id)
    end

    render :nothing=>true
  end

  def follow
    @img = Image.find_by_slug(params[:id] )

    @user = @img.user
    current_user.follow( @user )

    if @user.setting.newsletter?
      Notifier.delay.following(@img.id)
    end

  end

  def unfollow
    @img = Image.find_by_slug(params[:id] )

    @user = @img.user
    current_user.stop_following( @user )

  end

end
