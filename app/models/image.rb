class Image < ActiveRecord::Base
  mount_uploader :file, ImagesUploader
  before_save :generate_slugs
  belongs_to :user
  has_many :comments, :dependent => :destroy
  acts_as_taggable
  acts_as_voteable
  after_save :do_after

  def generate_slugs
    #Unqiue Slug
    loop do
      _slug = String.random_alphanumeric(7)
      if Image.find_by_slug(_slug) == nil
        self.slug = _slug
        break
      end
    end if self.slug.blank?

    self.delete_token = String.random_alphanumeric(7) if self.delete_token.blank?
    #self.md5=Digest::MD5.hexdigest( File.read( self.file.current_path ) ) if self.md5.blank?
    #Rails.cache.delete("gallery_images")
    #Rails.cache.delete("comments_image_#{self.id}")
  end

  def do_after
    if self.front_page?
      #send out front page newsletter
      if self.user.setting.newsletter? #send email
        Notifier.delay.front_page(self.id)
      end
    end
  end

end
