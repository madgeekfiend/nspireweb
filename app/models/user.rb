class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :omniauthable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :facebook_access_token, :facebook_id, :nickname, :name

  before_save :generate_token
  after_create :post_create
  has_many :images, :dependent => :destroy
  has_many :comments, :dependent => :destroy
  acts_as_voter
  has_karma(:images, :as=>:submitter, :weight=>0.3)
  has_karma(:comments, :as=>:submitter, :weight=>0.3)
  acts_as_follower
  acts_as_followable
  validates_uniqueness_of :nickname
  has_one :setting, :dependent => :destroy
  has_many :emails, :dependent => :destroy

  def avatar_profile_url
    if self.facebook_id.blank?
      "gravatar.png"
    else
      "http://graph.facebook.com/#{self.facebook_id}/picture"
    end
  end

  def has_nickname?
   not self.nickname.blank?
  end

  def generate_token
    self.mobile_token||=SecureRandom.hex(24) #if self.mobile_token.blank?

    loop do
      _slug = String.random_alphanumeric(7)
      if User.find_by_slug(_slug) == nil
        self.slug = _slug
        break
      end
    end if self.slug.blank?

  end

  def self.find_for_facebook_oauth(access_token, signed_in_resource=nil)
    data = access_token.extra.raw_info
    if user = User.where(:email => data.email).first
      user.facebook_access_token = access_token['credentials']['token']
      if user.full_size_image_slots != 3 or user.email_slots != 6
        user.full_size_image_slots=3
        user.email_slots = 6
        user.save!
      end
      user
    else # Create a user with a stub password.
      pword = Devise.friendly_token[0,8]
      user = User.new(:email => data.email, :password => pword, :facebook_access_token=>access_token['credentials']['token'], :facebook_id => access_token['uid'], :name=>data['name'] )
      user.confirm!
      Notifier.delay.send_password_facebook_login(pword,data.email)
      if user.full_size_image_slots != 3 or user.email_slots != 6
        user.full_size_image_slots=3
        user.email_slots = 6
        user.save!
      end
      user # If account is through facebook I don't want to send out a confirm email thats gay
    end
  end

  def facebook
    @fb_user ||= FbGraph::User.me(current_user.facebook_access_token)
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"]
      end
      user.facebook_access_token = session["devise.facebook_data"]['credentials']['token'] if data = session["devise.facebook_data"] && session["devise.facebook_data"]['credentials']['token']
    end
  end

  private

  def post_create
    # I want to create the user settings
    self.setting = Setting.new
  end

end
