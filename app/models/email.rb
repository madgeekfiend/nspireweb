class Email < ActiveRecord::Base
  belongs_to :user
  validates_uniqueness_of :email, :scope=>:user_id

  def generate_token
    self.verify_token||= SecureRandom.hex(24)
  end

end
