class News < ActiveRecord::Base
  after_save :do_after


  def do_after
    Rails.cache.delete("news_feed")
  end

end
