class Comment < ActiveRecord::Base
  belongs_to :image
  belongs_to :user
  acts_as_voteable
  validates_length_of :body, :maximum=>255, :message=>"Comments must be less then 255 characters", :allow_blank => false
  after_save :update_cache

  def avatar_profile_url
    if self.user_id.blank?
      "gravatar.png"
    else
      user = User.find self.user_id

      if user.facebook_id.present?
        "http://graph.facebook.com/#{user.facebook_id}/picture"
      else
        "gravatar.png"
      end
    end
  end

  def update_cache
    Rails.cache.delete("comments_image_#{self.image_id}")
  end
end
