class Notifier < ActionMailer::Base
  default from: "admin@nsprme.com"

  def add_email user, email
    @user = user
    @code = email.verify_token
    @to_email = email.email
    mail(:to=>@to_email, :subject => "Someone at #{user.email} would like to share some images with you!")
  end

  def send_password_facebook_login password, email
    @password = password
    @to_email = email
    mail(:to=>@to_email, :subject => "Your password for logging in via mobile from nsprme.com" )
  end

  def send_inspired image_id
    @image = Image.find image_id
    mail( :to=>@image.user.email, :subject=>"Your photo inspired someone on nsprme.com!", :from=>"support@nsprme.com")
  end

  def comment_added image_id
    @image = Image.find image_id
    mail( :to=>@image.user.email, :subject=>"Someone left a comment on your photo at nsprme.com!", :from=>"support@nsprme.com")
  end

  def following image_id
    @image = Image.find image_id
    mail( :to=>@image.user.email, :subject=>"Someone has followed you on nsprme.com!", :from=>"support@nsprme.com")
  end

  def front_page image_id
    @image = Image.find image_id
    mail( :to=>@image.user.email, :subject=>"You made the frontpage of nsprme.com!", :from=>"support@nsprme.com")
  end

end
