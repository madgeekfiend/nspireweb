class NewsletterService
  include HTTParty
  base_uri "http://us4.api.mailchimp.com/1.3/"

  LIST_ID = "d62e7300ad"
  API_KEY = "10919600ac81c4956a3a9c2a5c57bb38-us4"

  def self.subscribe user
    options = { :apikey=>API_KEY, :id=>LIST_ID, :email_address=>user.email }
    ap options
    self.post("?method=listSubscribe", :query=>options)
  end

  def self.lists
    self.post("?method=lists", :query=>{:apikey=>API_KEY})
  end

  def self.unsubscribe user
    options = { :apikey=>API_KEY, :id=>LIST_ID, :email_address=>user.email, :delete_member=>"true" }
    self.post("?method=listUnsubscribe", :query=>options )
  end

end