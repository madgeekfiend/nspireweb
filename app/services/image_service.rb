class ImageService

  def self.addFavorite image, user
    # Add the image to a favorite set
    #REDIS.sadd( "user_favorite_#{user_id}", slug )
    #REDIS.sadd( "image_favorite_#{slug}", user_id )
    user.vote_for(image)

  end

  def self.get_favorite_count_by_image slug
    REDIS.scard "image_favorite_#{slug}"
  end

  def self.has_user_favorited? user_id, slug
    REDIS.sismember("image_favorite_#{slug}", user_id)
  end

  def self.unfavorite_image slug, user_id
    REDIS.srem("image_favorite_#{slug}", user_id)
    REDIS.srem("user_favorite_#{user_id}", slug)
  end

end