class AddFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :full_size_image_slots, :integer, :default=>1
    add_column :users, :use_iphone, :boolean, :default=>true
    add_column :users, :points, :integer, :default=>0
    add_column :users, :facebook_access_token, :string
    add_column :users, :facebook_id, :string, :default=>""
    add_column :users, :nickname, :string, :default=>nil
    add_column :users, :name, :string
    add_column :users, :slug, :string
    add_column :users, :mobile_apn_token, :string
    add_column :users, :banned, :boolean, :default=>false
    add_column :users, :email_slots, :integer, :default=>3

    add_index :users, :slug
    add_index :users, :nickname
  end
end
