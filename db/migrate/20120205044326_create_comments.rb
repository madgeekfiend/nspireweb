class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :body
      t.integer :image_id
      t.integer :user_id
      t.boolean :deleted, :default=>false
      t.string :facebook_id, :default=>nil
      t.integer :submitter_id
      t.timestamps
    end

    add_index :comments, :image_id
  end
end
