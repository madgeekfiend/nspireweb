class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.integer :user_id
      t.text :profile
      t.boolean :newsletter, :default=>true
      t.boolean :enable_push_notification, :default=>true
      t.boolean :push_comment, :default=>true
      t.boolean :push_follow, :default=>true

      t.timestamps
    end
  end
end
