class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :headline
      t.text :body
      t.boolean :active, :default=>true

      t.timestamps
    end
  end
end
