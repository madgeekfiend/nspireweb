class AddCoordinatesToImage < ActiveRecord::Migration
  def change
    add_column :images, :latitude, :decimal, :precision=>15, :scale=>10
    add_column :images, :longitude, :decimal, :precision=>15, :scale=>10
  end
end
