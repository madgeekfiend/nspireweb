class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :file
      t.string :slug, :limit=>12
      t.string :delete_token, :limit=>12
      t.string :fingerprint
      t.integer :user_id
      t.integer :weight
      t.boolean :featured
      t.string :caption
      t.boolean :front_page, :default=>false
      t.integer :submitter_id
      t.timestamps
    end

    add_index :images, :fingerprint
    add_index :images, :slug
  end
end
