class ChangeDefaultCurated < ActiveRecord::Migration
  def change
    change_column_default :users, :curated, true
  end
end
