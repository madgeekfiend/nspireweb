class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.string :email
      t.string :verify_token
      t.timestamp :verified_at
      t.integer :user_id

      t.timestamps
    end
  end
end
