Clique::Application.routes.draw do
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }
  devise_for :admins, :except=>[:new, :create, :destroy, :update, :show, :edit]
  mount RailsAdmin::Engine => '/wxztswe1', :as => 'rails_admin'

  root :to => "home#index"

  get '/i/:id'=>"images#show", :as=>:image_slug
  resources :images, :except=>[:create, :new, :destroy, :update] do
    resources :comments, :except=>[:show, :new, :index, :update]
  end

  resources :faqs, :only=>[:index], :as=>:faq
  get '/mobile/user' => "Mobile::User#index", :as=>:mobile_user_show
  get '/mobile/top' => "Mobile::User#top_nspre", :as=>:top_nspre
  get '/verify/email' => "Emails#verify_other", :as=>:verify_other_email

  resource :setting, :only=>[:show,:edit, :update]
  #resource :email, :only=>[:edit,:create,:destroy]

  match '/mobile/register' => "api::Mobile#register", :as=>:api_mobile_register
  post '/mobile/image/upload' =>"api::Mobile#image", :as=>:api_mobile_image_upload

  get '/dashboard' => "dashboard#index", :as=>:dashboard

  # User stuff
  get '/profile/:id' => "users#profile", :as=>:profile

  #static pages
  get '/noprofile' => "static_pages#no_profile", :as=>:no_profile

  post '/search'=>"search#show", :as=>:search

  # AJAX calls
  post '/images/favorite/:id' => "images#favorited", :as=>:favorite_image
  post '/image/follow/:id' => "images#follow", :as=>:follow_user
  post '/image/unfollow/:id' => "images#unfollow", :as=>:unfollow_user

  get '/about'=>"static_pages#about", :as=>:about

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'


  # This is almost a catch all so make sure this stays at the bottom
  get '/:slug' => "images#thumb", :as=>:display_thumb
end
